<?php
class Database {
    protected PDO $pdo;
    protected $dataBag = array();
    protected string $table;
    protected string $where;

    public function __construct($host = 'localhost', $db = 'AUTH', $port = 3306, $username = 'Jayesh', $password = 'Jayesh', $fetchMode = PDO::FETCH_OBJ)
    {
        $this->pdo = new PDO("mysql:host={$host};port={$port};dbname={$db}", $username, $password);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, $fetchMode);
        $this->where = "1";
    }

    /**
     * Used to hit DDL Statements
     */
    public function query($sql)
    {
        return $this->pdo->query($sql);
    }

    /**
     * Use this to fire Raw Select Queries
     */
    public function rawQueryExecutor($sql)
    {
        return $this->query($sql)->fetchAll();
    }

    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    public function where(string $field, string $value, string $operator="=")
    {
        $this->dataBag["WHERE$field"] = $value;
        if($this->where === "1") {
            $this->where = "$field $operator :WHERE$field"; // yaha pe WHERE$field me pura value aayega bheja hua. i.e. shadab.
        } else {
            $this->where = $this->where . " AND $field $operator :WHERE$field"; // yaha pe WHERE$field me pura value aayega bheja hua. i.e. shadab.
        }
        return $this;
    }

    public function get($columns = "*")
    {
        $sql = $this->prepareSQLQuery($columns);
        $ps = $this->pdo->prepare($sql);
        $ps->execute($this->dataBag);
        $this->resetFields();
        return $ps->fetchAll();
    }

    public function first($columns = "*")
    {
        $sql = $this->prepareSQLQuery($columns);
        $sql = $sql . " LIMIT 0, 1";
        $ps = $this->pdo->prepare($sql);
        $ps->execute($this->dataBag);
        $this->resetFields();
        $data = $ps->fetchAll();
        return !empty($data) ? $data[0] : null;
    }

    public function count() 
    {
        $as = "count";
        $sql = "SELECT count(*) as $as FROM {$this->table} WHERE {$this->where}";
        $ps = $this->pdo->prepare($sql);
        $ps->execute($this->dataBag);
        $this->resetFields();
        return $ps->fetchAll()[0]->$as;
    }

    public function exists(mixed $data)
    {
        foreach($data as $key=>$value) {
            $this->where($key, $value);
        }
        return $this->count() >= 1 ? true : false;
    }

    public function insert($data) 
    {
        $keys = array_keys($data);

        $fields = "`" . implode("`, `", $keys) . "`";
        $placeholder = ":". implode(", :", $keys);

        $sql = "INSERT INTO {$this->table} ($fields) VALUES($placeholder)";
        $ps = $this->pdo->prepare($sql);
        $this->resetFields();
        return $ps->execute($data);
    }

    public function update(array $data)
    {
        $updationString = "";
        foreach($data as $key=>$value) {
            $updationString = $updationString . " $key = :$key,";
        }
        $updationString = rtrim($updationString, ",");
        $sql = "UPDATE {$this->table} SET {$updationString} WHERE {$this->where}";
        $dataWithConditionalParams = array_merge($data, $this->dataBag);
        $ps = $this->pdo->prepare($sql);
        $this->resetFields();
        return $ps->execute($dataWithConditionalParams);
    }

    public function delete()
    {
        $sql = "DELETE FROM {$this->table} WHERE {$this->where}";
        $dataparams = $this->dataBag;
        $ps = $this->pdo->prepare($sql);
        $this->resetFields();
        return $ps->execute($dataparams);
    }

    public function resetFields()
    {
        $this->table = "";
        $this->where = "1";
        $this->dataBag = array();
    }

    private function prepareSQLQuery($columns) 
    {
        return "SELECT $columns FROM {$this->table} WHERE {$this->where}";  // yaha pe $this-where me username = WHEREusername aayega.
    }
}
?>