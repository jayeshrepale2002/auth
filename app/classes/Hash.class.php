<?php

class Hash
{
    static public function make(string $plaintext): string
    {
        return password_hash($plaintext, PASSWORD_BCRYPT);
    }

    static public function verify(string $password, string $hashedPassword): bool
    {
        return password_verify($password, $hashedPassword);
    }
}
?>