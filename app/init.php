<?php
$app = __DIR__;

require_once("$app/helper_functions/utility.php");
require_once("$app/classes/Hash.class.php");
require_once("$app/classes/ErrorHandler.class.php");
require_once("$app/classes/Validator.class.php");
require_once("$app/classes/Database.class.php");
require_once("$app/classes/User.class.php");

$database = new Database();
User::$database = $database;
$validator = new  Validator($database);
User::build();
?>