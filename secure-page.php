<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Secured Page</title>
</head>
<body>
    <h1>Welcome to the Secured Page!</h1>
    <p>You have access to this page.</p>
</body>
</html>