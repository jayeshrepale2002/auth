<?php
require_once('./app/init.php');
if(isset($_POST['signup']))
{
    $validator->check($_POST, [
        'email' => [
            'required' => true,
            'email' => true,
            'minlength' => 5,
            'maxlength' => 225,
            'unique' => 'users'
        ],
        'username' => [
            'required' => true,
            'maxlength' => 20,
            'unique' => 'users'
        ],
        'password' => [
            'required' => true,
            'minlength' => 8,
            'password' => true
        ]
        ]);

        if(!$validator->fails()) {
            User::create([
                'username' => $_POST['username'],
                'email' => $_POST['email'],
                'password' => $_POST['password']
            ]);

            redirect("secure-page.php");
        }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signup</title>
</head>
<body>
    <div style="margin: 40px auto; width: 600px;">
        <h1>Sign Up</h1>
        <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">
            <!-- email
            username
            password        -->
            <div>
                <label for="username">Username</label>
                <input type="text" name="username" id="username">
                <span><?=$validator->errors()->has('username') ? $validator->errors()->first('username') : '';?></span>
            </div>
            <br>
            <div>
                <label for="email">Email</label>
                <input type="email" name="email" id="email">
                <span><?=$validator->errors()->has('email') ? $validator->errors()->first('email') : '';?></span>
            </div>
            <br>
            <div>
                <label for="password">Password</label>
                <input type="password" name="password" id="password">
                <span><?=$validator->errors()->has('password') ? $validator->errors()->first('password') : '';?></span>
            </div>
            <br>
            <div>
                <input type="submit" name="signup">
            </div>
        </form>
    </div>
</body>
</html>